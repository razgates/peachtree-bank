const gulp = require('gulp');
const concat = require('gulp-concat');
const path = require('path');
const browserSync = require('browser-sync').create();
const less = require('gulp-less');
const runSequence = require('run-sequence');
const angularFilesort = require('gulp-angular-filesort');
const inject = require('gulp-inject');
const wiredep = require('wiredep').stream;
const mainBowerFiles = require('main-bower-files');
const flatten = require('gulp-flatten');
const replace = require('gulp-replace');

var devMode = false;

var wiredepConf = {
    exclude: [/\/bootstrap\.js$/, /\/bootstrap\.css/],
    directory: 'bower_components'
};

gulp.task('styles', function() {
    var injectFiles = gulp.src([
        path.join('./src/**/*.less'),
        path.join('!./src/index.less')
    ], { read: false });

    var injectOptions = {
        starttag: '// css:injector',
        endtag: '// css:endinjector',
        addRootSlash: false
    };

    var lessOptions = {
        paths: ['./src/**/*.less'],
        relativeUrls: true
    };

    gulp.src('./src/index.less')
        .pipe(inject(injectFiles, injectOptions))
        .pipe(wiredep(Object.assign({}, wiredepConf)))
        .pipe(less(lessOptions))
        .pipe(replace('../bower_components/bootstrap/fonts/', '../fonts/'))
        .pipe(gulp.dest('./dist/styles'))
        .pipe(browserSync.reload({
            stream: true
        }));

    // Add fonts
    gulp.src('./bower_components/**/*.{eot,svg,ttf,woff,woff2}')
        .pipe(flatten())
        .pipe(gulp.dest('./dist/fonts'));
});

gulp.task('scripts', function() {
    var vendorScripts = function() {
        var scripts = mainBowerFiles().filter(function(filename) {
            return filename.match(/.+\.js$/)
        });
        return scripts;
    };

    // Vendor js files
    gulp.src(vendorScripts())
        .pipe(concat('vendor.js'))
        .pipe(gulp.dest('./dist/scripts'))
        .pipe(browserSync.reload({
            stream: true
        }));

    // App js files
    gulp.src('./src/**/*.js')
        .pipe(angularFilesort())
        .pipe(concat('index.js'))
        .pipe(gulp.dest('./dist/scripts'))
        .pipe(browserSync.reload({
            stream: true
        }));
});

gulp.task('html', function() {
    gulp.src('./src/**/*.html')
        .pipe(gulp.dest('./dist/'))
        .pipe(browserSync.reload({
            stream: true
        }));

    gulp.src('./src/assets/**/*')
        .pipe(gulp.dest('./dist/assets'));
});

gulp.task('build', function() {
    runSequence(['styles', 'scripts', 'html']);
});

gulp.task('browser-sync', function() {
    browserSync.init(null, {
        open: false,
        server: {
            baseDir: 'dist'
        }
    });
});

gulp.task('start', function() {
    devMode = true;
    gulp.start(['build', 'browser-sync']);
    gulp.watch(['./src/**/*.less'], ['styles']);
    gulp.watch(['./src/**/*.js'], ['scripts']);
    gulp.watch(['./src/**/*.html'], ['html']);
});