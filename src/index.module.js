(function () {
    'use strict';

    angular
        .module('peachtree', ['ui.router', 'ui.bootstrap', 'angularMoment'])
})();
