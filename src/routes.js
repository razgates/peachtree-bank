angular
    .module('peachtree')
    .config(routesConfig);

/** @ngInject */
function routesConfig($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider.otherwise('/');

    $stateProvider
        .state('main', {
            url: '/',
            templateUrl: 'app/components/main/main.html',
            controller: 'MainController',
            controllerAs: 'mainCtrl',
            redirectTo: 'main.account'
        })
        .state('main.account', {
            url: 'account',
            templateUrl: 'app/components/main/account-view/account-view.html',
            controller: 'AccountViewController',
            controllerAs: 'accountViewCtrl'
        })
    ;
}
