(function () {
	'use strict';

	angular
		.module('peachtree')
		.component('ptHeader', {
			templateUrl: 'app/components/shared/header/header.html',
			controller: 'PTHeaderController',
			controllerAs: 'headerCtrl'
		});
})();
