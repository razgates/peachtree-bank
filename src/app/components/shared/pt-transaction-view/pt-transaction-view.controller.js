(function() {
    'use strict';

    angular
        .module('peachtree')
        .controller('PTTransactionViewController', PTTransactionViewController);

    /** @ngInject */
    function PTTransactionViewController(TransactionManagerService, Transaction) {
        var vm = this;

        vm.$onInit = onInit;

        vm.getTransactions = getTransactions;
        vm.sortByAction = sortByAction;
        vm.setSortByVars = setSortByVars;
        vm.clearAllSortByStates = clearAllSortByStates;
        vm.clearSearch = clearSearch;

        function onInit() {
            vm.getTransactions();

            vm.setSortByVars();
            // Default sortBy
            vm.sortBy = vm.sortByTransactionDate.orderBy;
        }

        /*
         * sortByAction
         * @description         Update state and transaction list order for sorting button that is clicked
         * @param fieldVar      Obj - sortByTransactionDate, sortByBeneficiary or sortByAmount
         */
        function sortByAction(fieldVar) {
            if (fieldVar.state) {
                fieldVar.state = !fieldVar.state;
            } else if (fieldVar.state === false) {
                fieldVar.state = null;
                fieldVar.orderBy = null;
            } else if (fieldVar.state === null) {
                fieldVar.state = true;
            }

            if (fieldVar.state || fieldVar.state === false) {
                var sign = (fieldVar.state) ? '' : '-';
                fieldVar.orderBy = sign + fieldVar.name;
            }

            vm.clearAllSortByStates(fieldVar);
            vm.sortBy = fieldVar.orderBy;
        }

        /*
         * clearAllSortByStates
         * @description         Clear states of all sorting variable apart from the variable passed through
         * @param fieldVar      Obj - sortByTransactionDate, sortByBeneficiary or sortByAmount
         */
        function clearAllSortByStates(fieldVar) {
            if (fieldVar !== vm.sortByTransactionDate) {
                vm.sortByTransactionDate.state = null;
            }
            if (fieldVar !== vm.sortByBeneficiary) {
                vm.sortByBeneficiary.state = null;
            }
            if (fieldVar !== vm.sortByAmount) {
                vm.sortByAmount.state = null;
            }
        }

        /*
         * clearSearch
         * @description          Clear search input text
         */
        function clearSearch() {
            vm.search = '';
        }

        /*
         * setSortByVars
         * @description          Set sorting variables to default settings
         */
        function setSortByVars() {
            vm.sortByTransactionDate = {
                name: 'transactionDate',
                state: false,
                orderBy: '-transactionDate'
            };

            vm.sortByBeneficiary = {
                name: 'merchant',
                state: null,
                orderBy: null
            };

            vm.sortByAmount = {
                name: 'amount',
                state: null,
                orderBy: null
            };
        }

        /*
         * getTransactions
         * @description         Get all transactions from mock data in transactions.json
         */
        function getTransactions() {
            var response = TransactionManagerService.getTransactions();
            if (angular.isDefined(response) && response.length !== 0) {
                var transactionArray = [];
                angular.forEach(response.data, function(transaction) {
                    var mappedTransaction = Transaction.build(transaction);
                    if ((angular.isDefined(mappedTransaction)) && (!jQuery.isEmptyObject(mappedTransaction))) {
                        transactionArray.push(mappedTransaction);
                    }
                });

                TransactionManagerService.transactions = transactionArray;
                vm.transactions = TransactionManagerService.transactions;
            } else {
                // There are no transactions
                vm.transactions = [];
                alert('Could not get transactions. Please try again later.');
            }
        }
    }
})();