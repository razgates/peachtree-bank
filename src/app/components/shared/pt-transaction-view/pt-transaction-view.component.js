(function () {
	'use strict';

	angular
		.module('peachtree')
		.component('ptTransactionView', {
			templateUrl: 'app/components/shared/pt-transaction-view/pt-transaction-view.html',
			controller: 'PTTransactionViewController',
			controllerAs: '$ctrl'
		});
})();
