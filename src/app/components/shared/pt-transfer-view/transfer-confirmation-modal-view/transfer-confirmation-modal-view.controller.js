(function () {
    'use strict';

    angular
        .module('peachtree')
        .controller('TransferConfirmationModalViewController', TransferConfirmationModalViewController);

    /** @ngInject */
    function TransferConfirmationModalViewController($uibModalInstance, modalData, Transaction, TransactionManagerService) {
        var vm = this;

        vm.$onInit = onInit;

        vm.handleClose = handleClose;
        vm.handleDismiss = handleDismiss;
        vm.handleSubmit = handleSubmit;

        vm.transferMoney = transferMoney;

        /** ******************* */

        function onInit() {
            vm.title = modalData.title;
            vm.bodyText = modalData.bodyText;
            vm.state = modalData.state;
            vm.item = modalData.item;
        }

        function handleClose() {
            $uibModalInstance.close(false);
        }

        function handleSubmit() {
            vm.transferMoney();
        }

        function handleDismiss() {
            $uibModalInstance.dismiss('cancel');
        }

        /** ******************* */

        /*
         * transferMoney
         * @description         Add transaction to transaction list when transfer is confirmed and close modal view.
         */
        function transferMoney() {
            var transaction = Transaction.build();
            transaction.transactionDate = new Date();
            transaction.merchant = vm.item.to;
            transaction.amount = vm.item.amount;
            transaction.merchantLogo = 'assets/images/icons/peach_icon.jpg';

            TransactionManagerService.transactions.push(transaction);
            $uibModalInstance.close(true);
        }
    }
})();
