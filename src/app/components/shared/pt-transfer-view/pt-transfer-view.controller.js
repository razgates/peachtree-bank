(function () {
    'use strict';

    angular
        .module('peachtree')
        .controller('PTTransferViewController', PTTransferViewController);

    /** @ngInject */
    function PTTransferViewController($uibModal) {
        var vm = this;

        vm.$onInit = onInit;

        vm.transferMoney = transferMoney;
        vm.openConfirmationModalView = openConfirmationModalView;

        function onInit() {
            vm.accountTotalAmount = 5824.76;
            vm.transfer = {
                from: 'Free Checking(4692) - $' + vm.accountTotalAmount
            };
        }

        /*
         * transferMoney
         * @description         Action when transfer button is clicked
         */
        function transferMoney() {
            vm.openConfirmationModalView(vm.transfer);
        }

        /*
         * openConfirmationModalView
         * @description         Open confirmation modal view for money transfer
         */
        function openConfirmationModalView(transaction) {
            $uibModal.open({
                animation: true,
                templateUrl: 'app/components/shared/pt-transfer-view/transfer-confirmation-modal-view/transfer-confirmation-modal-view.html',
                controller: 'TransferConfirmationModalViewController',
                controllerAs: '$ctrl',
                size: 'sm',
                resolve: {
                    modalData: function () {
                        return {
                            title: 'Transfer Confirmation',
                            bodyText: 'Confirm the transfer details below and click Transfer.',
                            state: 'primary',
                            item: transaction
                        };
                    }
                },
                windowClass: 'small-modal-window'
            }).result.then(function (success) {
                // Success
                if (success) {
                    vm.accountTotalAmount = vm.accountTotalAmount - transaction.amount;
                    vm.transfer = {
                        from: 'Free Checking(4692) - $' + vm.accountTotalAmount,
                        to: '',
                        amount: null
                    };
                    vm.transferMoneyForm.$setPristine();
                }
            });
        }
    }
})();
