(function () {
	'use strict';

	angular
		.module('peachtree')
		.component('ptTransferView', {
			templateUrl: 'app/components/shared/pt-transfer-view/pt-transfer-view.html',
			controller: 'PTTransferViewController',
			controllerAs: '$ctrl'
		});
})();
