(function () {
    'use strict';

    angular
        .module('peachtree')
        .controller('MainController', MainController);

    /** @ngInject */
    function MainController() {
        var vm = this;

        vm.$onInit = onInit;

        function onInit() {
        }
    }
})();
