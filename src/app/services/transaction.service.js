(function() {
    'use strict';

    angular
        .module('peachtree')
        .factory('Transaction', Transaction);

    /** @ngInject */
    function Transaction() {
        Transaction.build = build;

        return Transaction;

        /**
         * Constructor, with class name
         */
        function Transaction(obj) {
            var data = (angular.isDefined(obj));
            if (data) {
                this.amount = parseFloat(obj.amount, 10);
                this.categoryCode = obj.categoryCode;
                this.merchant = obj.merchant;
                this.merchantLogo = obj.merchantLogo;
                this.transactionDate = obj.transactionDate;
                this.transactionType = obj.transactionType;
            }
            // Create new Transaction object
            else {
                this.amount = 0;
                this.categoryCode = '#12a580';
                this.merchant = '';
                this.merchantLogo = '';
                this.transactionDate = null;
                this.transactionType = 'Bank Transfer';
            }
        }

        function build(data) {
            return new Transaction(data);
        }
    }
})();