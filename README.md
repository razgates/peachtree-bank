# README #

### What is this repository for? ###
Backbase Front End Assignment: Make A Transaction

### How do I get set up? ###
Run 'gulp build' on the directory root.
Open index.html in the 'dist' folder and it should work locally on your browser (fingers crossed).

### Technologies used
* Angular 1.5 >
* Gulp
	* To compile LESS files for styling
	* To concat all app and vendor code from bower components into JS files
	* To allow me to test my changes on a server with a watch on my JS, HTML and CSS files (to speed up the coding process)

### App setup
* I split up my components and controllers into two folders 'Main' and 'Shared'. 
* 'Main' includes the fixed main view which has a static header (with Peachtree logo) and a dynamic UIView that is used for the page view. In this case it's the account view. I used this setup to split up the code in a clean and efficient way and to make it scalable.
* 'Shared' includes the shared components that can be re-used across the whole app. For the account view I've added the 'header', 'transfer view', and 'transaction view' components.
* The 'Service' folder includes a Transaction service which is used to create Transaction objects through existing data or none at all.
* The 'Transaction Manager Service' is used to store the Transaction objects built from 'Transaction' and list all requests that can be made to the backend (in this case it would be getTransactions which pulls a JSON object locally).
* I re-created the briefcase and arrows images using Affinity Designer to make them more defined (less pixelated) and to give it a transparent background.

Author: Rozh Surchi